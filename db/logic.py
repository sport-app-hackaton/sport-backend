import datetime
from hashlib import sha512

from sqlalchemy.orm import Session

from db import models
from db.crud import get_user_by_email
from utils import hash_password


def login(db: Session, email: str, password: str) -> models.User | None:
    user = get_user_by_email(db, email)
    password_hash = hash_password(password)
    true_hash = user.hashed_password
    if password_hash == true_hash:
        return user
    else:
        return None


def refresh_token(db: Session, email: str, password: str) -> models.User | None:
    user = get_user_by_email(db, email)
    password_hash = hash_password(password)
    true_hash = user.hashed_password
    if password_hash == true_hash:
        base = '_'.join([user.first_name, user.middle_name, user.last_name, user.email, password,
                         str(datetime.datetime.now())]).lower()

        user.personal_token = sha512(base.encode('utf-8')).hexdigest()
        db.commit()
        db.refresh(user)
        return user
    else:
        return None
