from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Date
from sqlalchemy.orm import relationship

from .database import Base


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    first_name = Column(String)
    last_name = Column(String)
    middle_name = Column(String)
    bdate = Column(Date)
    email = Column(String, unique=True, index=True)
    phone_number = Column(String)
    photo_path = Column(String)
    location = Column(String)
    is_male = Column(Boolean, default=True)
    hashed_password = Column(String)
    personal_token = Column(String)
    is_active = Column(Boolean, default=True)
    role = Column(String, default='sportsman')

    events = relationship("Event", back_populates="owner")
    participation = relationship('Participant', back_populates="user")


class Event(Base):
    __tablename__ = "events"

    id = Column(Integer, primary_key=True)
    date = Column(Date)
    title = Column(String, index=True)
    description = Column(String, index=True)
    owner_id = Column(Integer, ForeignKey("users.id"))
    location = Column(String)
    tag_id = Column(Integer, ForeignKey("tags.id"))

    owner = relationship("User", back_populates="events")
    participants = relationship('Participant', back_populates="event")
    tag = relationship('Tag', back_populates='events')


class Participant(Base):
    __tablename__ = "participants"

    id = Column(Integer, primary_key=True)

    user_id = Column(Integer, ForeignKey("users.id"))
    event_id = Column(Integer, ForeignKey("events.id"))
    result = Column(Integer, default=0)
    place = Column(Integer, default=1)

    user = relationship('User', back_populates='participation')
    event = relationship('Event', back_populates='participants')


class Tag(Base):
    __tablename__ = 'tags'

    id = Column(Integer, primary_key=True)

    title = Column(String)

    events = relationship('Event', back_populates='tag')
    news = relationship('News', back_populates='tag')


class News(Base):
    __tablename__ = "news"

    id = Column(Integer, primary_key=True)
    title = Column(String, index=True)
    content = Column(String)
    pub_date = Column(Date)
    tag_id = Column(Integer, ForeignKey("tags.id"))

    tag = relationship('Tag', back_populates='news')
