import datetime
from hashlib import sha512

from db import schemas


def hash_password(password):
    return sha512(password.encode('utf-8')).hexdigest()


def generate_token(user: schemas.UserCreate) -> str:
    base = '_'.join([user.first_name, user.middle_name, user.last_name, user.email, user.password, str(datetime.datetime.now())]).lower()
    return sha512(base.encode('utf-8')).hexdigest()
