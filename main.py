from fastapi import Depends, FastAPI, HTTPException
from sqlalchemy.orm import Session
from fastapi.middleware.cors import CORSMiddleware

from db import crud, models, schemas, logic
from db.database import SessionLocal, engine

models.Base.metadata.create_all(bind=engine)

app = FastAPI()

origins = [
    "http://localhost",
    "http://localhost:3000",
    "http://127.0.0.1:8000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.post("/users/login/", response_model=schemas.LoggedUser, tags=["users"])
def login(login_data: schemas.LoginSchema, db: Session = Depends(get_db)):
    user = logic.login(
        db=db,
        email=login_data.email,
        password=login_data.password
    )
    if user is None:
        return {'success': False, 'message': 'Invalid Credentials'}
    else:
        return user


@app.post("/users/refresh_token/", response_model=schemas.LoggedUser, tags=["users"])
def refresh_user_token(login_data: schemas.LoginSchema, db: Session = Depends(get_db)):
    user = logic.refresh_token(
        db=db,
        email=login_data.email,
        password=login_data.password
    )
    if user is None:
        return {'success': False, 'message': 'Invalid Credentials'}
    else:
        return user


@app.post("/users/", response_model=schemas.LoggedUser, tags=["users"])
def create_user(user: schemas.UserCreate, db: Session = Depends(get_db)):
    db_user = crud.get_user_by_email(db, email=user.email)
    if db_user:
        raise HTTPException(status_code=400, detail="Email already registered")
    return crud.create_user(db=db, user=user)


@app.get("/users/", response_model=list[schemas.User], tags=["users"])
def read_users(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    users = crud.get_users(db, skip=skip, limit=limit)
    return users


@app.get("/users/{personal_token}", response_model=schemas.User, tags=["users"])
def read_user(personal_token: str, db: Session = Depends(get_db)):
    db_user = crud.get_user_by_token(db, personal_token)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_user

@app.get("/users/{user_id}", response_model=schemas.User, tags=["users"])
def read_user(user_id: int, db: Session = Depends(get_db)):
    db_user = crud.get_user(db, user_id=user_id)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_user


@app.post("/tags/", response_model=schemas.Tag, tags=["tags"])
def create_tag(tag: schemas.TagBase, db: Session = Depends(get_db)):
    db_tag = crud.get_tag_by_title(db, title=tag.title)
    if db_tag:
        raise HTTPException(status_code=400, detail="Tag already registered")
    return crud.create_tag(db=db, tag=tag)


@app.get("/tags/", response_model=list[schemas.Tag], tags=["tags"])
def read_tags(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    tags = crud.get_tags(db, skip=skip, limit=limit)
    return tags


@app.get("/tags/{tag_id}", response_model=schemas.Tag, tags=["tags"])
def read_tag(tag_id: int, db: Session = Depends(get_db)):
    db_tag = crud.get_tag(db, tag_id=tag_id)
    if db_tag is None:
        raise HTTPException(status_code=404, detail="Tag not found")
    return db_tag


@app.post("/news/", response_model=schemas.Tag, tags=["news"])
def create_post(post: schemas.NewsBase, db: Session = Depends(get_db)):
    tag_id = post.tag_id
    if tag_id != 0:
        db_tag = crud.get_tag(db, tag_id=tag_id)
        if db_tag is None:
            raise HTTPException(status_code=404, detail="Tag not found")
    return crud.create_post(db=db, post=post)


@app.get("/news/", response_model=list[schemas.News], tags=["news"])
def read_posts(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    posts = crud.get_posts(db, skip=skip, limit=limit)
    return posts


@app.get("/news/{post_id}", response_model=schemas.News, tags=["news"])
def read_post(post_id: int, db: Session = Depends(get_db)):
    db_post = crud.get_post(db, post_id=post_id)
    if db_post is None:
        raise HTTPException(status_code=404, detail="News not found")
    return db_post


@app.post("/events/", response_model=schemas.Event, tags=["events"])
def create_event(event: schemas.EventCreate, db: Session = Depends(get_db)):
    owner_token = event.owner_token
    owner = crud.get_user_by_token(db, owner_token)
    if owner is None:
        raise HTTPException(status_code=404, detail="User not found")
    return crud.create_event(db=db, event=event)


@app.get("/events/", response_model=list[schemas.Event], tags=["events"])
def read_events(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    events = crud.get_events(db, skip=skip, limit=limit)
    return events


@app.get("/events/{event_id}", response_model=schemas.Event, tags=["events"])
def read_event(event_id: int, db: Session = Depends(get_db)):
    db_event = crud.get_event(db, event_id=event_id)
    if db_event is None:
        raise HTTPException(status_code=404, detail="Event not found")
    return db_event


@app.post('/events/applies/', response_model=schemas.Participant, tags=["applies"])
def create_apply(apply: schemas.ParticipantCreate, db: Session = Depends(get_db)):
    apply_dict = apply.dict()
    user_token = apply_dict.pop('user_token')
    event_id = apply_dict.pop('event_id')
    user = crud.get_user_by_token(db, user_token)
    event = crud.get_event(db, event_id)

    if event is None:
        raise HTTPException(status_code=404, detail="Event not found")
    elif user is None:
        raise HTTPException(status_code=404, detail="User not found")
    else:
        apply_db = crud.get_applies_by_event_and_user(
            db=db,
            event_id=event_id,
            user_id=user.id
        )
        if apply_db is None:
            return crud.create_apply(db=db, apply=apply)
        else:
            if apply.result is None or apply.result is None:
                raise HTTPException(status_code=400,
                                    detail="Apply already exists and can not be modified with given data. Place and result must be passed")
            else:
                apply_db.result = apply.result
                apply_db.place = apply.place
                db.commit()
                db.refresh(apply_db)
                return apply_db


@app.get("/events/{event_id}/applies", response_model=list[schemas.Participant], tags=["applies"])
def get_event_applies(event_id: int, db: Session = Depends(get_db)):
    db_event = crud.get_event(db, event_id=event_id)
    if db_event is None:
        raise HTTPException(status_code=404, detail="Event not found")
    else:
        return crud.get_applies_by_event(db=db, event_id=event_id)


@app.get("/users/{user_id}/applies", response_model=list[schemas.Participant], tags=["applies"])
def get_user_applies(user_id: int, db: Session = Depends(get_db)):
    db_user = crud.get_user(db, user_id=user_id)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    else:
        return crud.get_applies_by_user(db=db, user_id=user_id)


@app.get("/applies/", response_model=list[schemas.Participant], tags=["applies"])
def get_applies(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    return crud.get_all_applies(db=db, skip=skip, limit=limit)


@app.get("/applies/get/", response_model=schemas.Participant, tags=["applies"])
def get_apply_by_event_and_user(event_id: int, user_id: int, db: Session = Depends(get_db)):
    user = crud.get_user(db, user_id)
    event = crud.get_event(db, event_id)

    if event is None:
        raise HTTPException(status_code=404, detail="Event not found")
    elif user is None:
        raise HTTPException(status_code=404, detail="User not found")
    else:
        return crud.get_applies_by_event_and_user(db=db, event_id=event_id, user_id=user_id)


@app.get("/applies/{apply_id}", response_model=schemas.Participant, tags=["applies"])
def get_apply(apply_id: int, db: Session = Depends(get_db)):
    return crud.get_apply(db=db, apply_id=apply_id)


@app.get("/events/from_to/", tags=["events", 'calendar'])
def get_events_by_period(period: schemas.PeriodSchema, db: Session = Depends(get_db)):
    if period.start is not None and period.end is not None:
        if period.start > period.end:
            raise HTTPException(status_code=400, detail="Period start must be smaller than period end")
        else:
            return db.query(models.Event).filter(models.Event.date >= period.start,
                                                 models.Event.date <= period.end).all()
    elif period.start is not None:
        return db.query(models.Event).filter(models.Event.date >= period.start).all()
    else:
        return db.query(models.Event).filter(models.Event.date == period.end).all()


@app.get("/news/from_to/", tags=["news", 'calendar'])
def get_news_by_period(period: schemas.PeriodSchema, db: Session = Depends(get_db)):
    if period.start is not None and period.end is not None:
        if period.start > period.end:
            raise HTTPException(status_code=400, detail="Period start must be smaller than period end")
        else:
            return db.query(models.News).filter(models.News.pub_date >= period.start,
                                                models.News.pub_date <= period.end).all()
    elif period.start is not None:
        return db.query(models.News).filter(models.News.pub_date >= period.start).all()
    else:
        return db.query(models.News).filter(models.News.pub_date == period.end).all()


@app.get("/news/tag/{tag}/", tags=["news"])
def get_news_by_tag(tag: str, db: Session = Depends(get_db)):
    db_tag = crud.get_tag_by_title(db, tag)
    if db_tag is None:
        raise HTTPException(status_code=404, detail="Tag not found")
    else:
        return db.query(models.News).filter(models.News.tag_id == db_tag.id).all()


@app.get("/events/tag/{tag}/", tags=["events"])
def get_events_by_tag(tag: str, db: Session = Depends(get_db)):
    db_tag = crud.get_tag_by_title(db, tag)
    if db_tag is None:
        raise HTTPException(status_code=404, detail="Tag not found")
    else:
        return db.query(models.Event).filter(models.Event.tag_id == db_tag.id).all()


@app.get("/users/{user_id}/events/", tags=["events",'users'])
def get_events_by_user(user_id: int, db: Session = Depends(get_db), active_only: bool = False):
    return crud.get_user_events(db=db, user_id=user_id, active_only=active_only)

@app.get("/events/{event_id}/users/", tags=["events",'users'])
def get_users_by_event(event_id: int, db: Session = Depends(get_db)):
    return crud.get_event_users(db=db, event_id=event_id)

@app.get("/news/search/title/", response_model=list[schemas.News], tags=["news", 'search'])
def find_news_by_title(search_query: str, db: Session = Depends(get_db)):
    return db.query(models.News).filter(models.News.title.contains(f"{search_query}")).all()


@app.get("/news/search/content/", response_model=list[schemas.News], tags=["news", 'search'])
def find_news_by_title(search_query: str, db: Session = Depends(get_db)):
    return db.query(models.News).filter(models.News.content.contains(f"{search_query}")).all()


@app.get("/events/search/title/", response_model=list[schemas.News], tags=["events", 'search'])
def find_news_by_title(search_query: str, db: Session = Depends(get_db)):
    return db.query(models.Event).filter(models.Event.title.contains(f"{search_query}")).all()


@app.get("/events/search/description/", response_model=list[schemas.News], tags=["events", 'search'])
def find_news_by_title(search_query: str, db: Session = Depends(get_db)):
    return db.query(models.Event).filter(models.Event.description.contains(f"{search_query}")).all()
