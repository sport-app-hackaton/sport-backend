import datetime
import random

from sqlalchemy.orm import Session

from utils import hash_password, generate_token
from . import models, schemas


def get_user(db: Session, user_id: int) -> models.User:
    return db.query(models.User).filter(models.User.id == user_id).first()


def get_user_by_email(db: Session, email: str) -> models.User:
    return db.query(models.User).filter(models.User.email == email).first()


def get_user_by_token(db: Session, token_: str) -> models.User:
    return db.query(models.User).filter(models.User.personal_token == token_).first()


def get_users(db: Session, skip: int = 0, limit: int = 100) -> list[models.User]:
    return db.query(models.User).offset(skip).limit(limit).all()


def create_user(db: Session, user: schemas.UserCreate) -> models.User:
    ptoken = generate_token(user)
    user = user.dict()
    pswd = user.pop('password')
    fake_hashed_password = hash_password(pswd)
    db_user = models.User(
        hashed_password=fake_hashed_password,
        personal_token=ptoken,
        **user,
    )
    print(user)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def get_tag(db: Session, tag_id: int):
    return db.query(models.Tag).filter(models.Tag.id == tag_id).first()


def get_tag_by_title(db: Session, title: str):
    return db.query(models.Tag).filter(models.Tag.title == title).first()


def get_tags(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.Tag).offset(skip).limit(limit).all()


def create_tag(db: Session, tag: schemas.TagBase):
    tag = tag.dict()
    db_tag = models.Tag(
        **tag
    )
    print(tag)
    db.add(db_tag)
    db.commit()
    db.refresh(db_tag)
    return db_tag


def get_post(db: Session, post_id: int):
    return db.query(models.News).filter(models.News.id == post_id).first()


def get_post_by_title(db: Session, title: str):
    return db.query(models.News).filter(models.News.title == title).first()


def get_posts(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.News).offset(skip).limit(limit).all()


def create_post(db: Session, post: schemas.NewsBase):
    post = post.dict()
    pub_date = datetime.date.today()
    db_post = models.News(
        **post,
        pub_date=pub_date
    )
    db.add(db_post)
    db.commit()
    db.refresh(db_post)
    return db_post


def get_event(db: Session, event_id: int):
    return db.query(models.Event).filter(models.Event.id == event_id).first()


def get_event_by_title(db: Session, title: str):
    return db.query(models.Event).filter(models.Event.title == title).first()


def get_events(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.Event).offset(skip).limit(limit).all()


def create_event(db: Session, event: schemas.EventCreate):
    edict = event.dict()
    owner = get_user_by_token(db, edict.pop('owner_token'))

    db_event = models.Event(
        **edict,
        owner_id=owner.id
    )

    db.add(db_event)
    db.commit()
    db.refresh(db_event)
    return db_event


def get_apply(db: Session, apply_id: int):
    return db.query(models.Participant).filter(models.Participant.id == apply_id).first()


def get_applies_by_event(db: Session, event_id: int):
    return db.query(models.Event).filter(models.Event.id == event_id).first().participants


def get_applies_by_user(db: Session, user_id: int):
    return db.query(models.User).filter(models.User.id == user_id).first().participation


def get_applies_by_event_and_user(db: Session, event_id: int, user_id: int):
    return db.query(models.Participant).filter(models.Participant.event_id == event_id).filter(
        models.Participant.user_id == user_id).first()


def get_all_applies(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.Participant).offset(skip).limit(limit).all()


def create_apply(db: Session, apply: schemas.ParticipantCreate):
    apply = apply.dict()
    user = get_user_by_token(db, apply.pop('user_token'))
    event = get_event(db, apply.pop('event_id'))

    db_apply = models.Participant(
        user_id=user.id,
        event_id=event.id
    )

    db.add(db_apply)
    db.commit()
    db.refresh(db_apply)
    return db_apply


def get_user_events(db: Session, user_id: int, active_only: bool = False):
    user_applies = get_applies_by_user(db, user_id)
    resp = []
    for apply in user_applies:
        event = apply.event
        if not active_only or (active_only and event.date >= datetime.date.today()):
            resp.append(event)
    return resp


def get_event_users(db: Session, event_id: int):
    event_applies = get_applies_by_event(db, event_id)
    resp = []
    for apply in event_applies:
        user = apply.user
        resp.append(user)
    return resp
