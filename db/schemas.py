import datetime
from typing import Optional

from pydantic import BaseModel


class LoginSchema(BaseModel):
    email: str
    password: str


class UserBase(BaseModel):
    email: Optional[str]
    first_name: Optional[str]
    last_name: Optional[str]
    middle_name: Optional[str]
    bdate: Optional[datetime.date]
    phone_number: Optional[str]
    photo_path: Optional[str]
    location: Optional[str]
    is_male: Optional[bool]
    role: Optional[str]


class UserCreate(UserBase):
    password: str


class User(UserBase):
    id: int
    is_active: bool


    class Config:
        orm_mode = True


class LoggedUser(User):
    personal_token: Optional[str]

    class Config:
        orm_mode = False


class TagBase(BaseModel):
    title: Optional[str]


class Tag(TagBase):
    id: int

    class Config:
        orm_mode = True


class NewsBase(BaseModel):
    title: Optional[str]
    content: Optional[str]
    tag_id: Optional[int]


class News(NewsBase):
    id: int

    class Config:
        orm_mode = True


class EventBase(BaseModel):
    title: Optional[str]
    description: Optional[str]
    date: Optional[datetime.date]
    location: Optional[str]
    tag_id: Optional[int]


class EventCreate(EventBase):
    owner_token: str


class Event(EventBase):
    id: int
    owner_id: int
    tag_id: Optional[int]

    class Config:
        orm_mode = True


class ParticipantBase(BaseModel):
    event_id: Optional[int]
    result: Optional[int] = None
    place: Optional[int] = None


class ParticipantCreate(ParticipantBase):
    user_token: str


class Participant(ParticipantBase):
    id: Optional[int]
    user_id: Optional[int]

    class Config:
        orm_mode = True


class PeriodSchema(BaseModel):
    start: Optional[datetime.date]
    end: Optional[datetime.date]
